<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

// $id = $_SESSION['order_id'];

$conn = connDB();

$productsOrders =  getProductOrders($conn);

date_default_timezone_set("Asia/Kuala_Lumpur");
$date = date("Y-m-d H:i:s"); 
// echo $date;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if(isset($_POST["refund_details"])){
        // $reject_reason_one = rewrite($_POST["reject_reason_one"]);
        $refund_method = rewrite($_POST["insert_refund_method"]);
        $refund_amount = rewrite($_POST["insert_refund_amount"]);
        $refund_note = rewrite($_POST["insert_refund_note"]);
        $refund_reason = rewrite($_POST["insert_refund_reason"]);

        $shipping_date = $date;
        $shipping_status = rewrite($_POST["shipping_status"]);
        // $shipping_method = rewrite($_POST["shipping_method"]);
        // $tracking_number = rewrite($_POST["tracking_number"]);

        $order_id = rewrite($_POST["order_id"]);
    }else{
        // $reject_reason_one = "";
        $refund_method = "";
        $refund_amount = "";
        $refund_note = "";
        $refund_reason = "";
        $shipping_date = "";
        $shipping_status = "";
        // $shipping_method = "";
        // $tracking_number = "";
        $order_id = "";
    }
}

$conn->close();
function promptError($msg)
{
    echo '<script>  alert("'.$msg.'");  </script>';
}

function promptSuccess($msg)
{
    echo '<script>  alert("'.$msg.'");   </script>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/shippingOut.php" />
    <meta property="og:title" content="Shipping Out | Boss" />
    <title>Shipping Out | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/shippingOut.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>



<div class="yellow-body padding-from-menu same-padding">

<form method="POST" action="utilities/refundShippingFunction.php">

    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">  
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">  
            Reject and Refund ( Order Number : #<?php echo $_POST['order_id'];?> )
        </a>  
    </h1>      

    <!-- <h4 class="h1-title h1-before-border shipping-h1">Reject or Refund</h4> -->

    <div class="width100 shipping-div2">
        <table class="details-table">
            <tbody>
            <?php 
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                //Order
                $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
                //OrderProduct
                $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
                //$orderDetails = $orderArray[0];
                
                if($orderArray != null)
                {
                    ?>
                    <tr>
                        <td>Bank Name</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getBankAccountHolder()?></td>
                    </tr>
                    <tr>
                        <td>Bank</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getBankName()?></td>
                    </tr>
                    <tr>
                        <td>Acc. No.</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getBankAccountNo()?></td>
                    </tr>
                    <tr>
                        <td>Total Product Price</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getSubtotal()?></td>
                    </tr>
                    <tr>
                        <td>Total Payment Amount</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getTotal()?></td>
                    </tr>
                    <?php
                    
                } 
            }       
            else
            {}
            $conn->close();
            ?>
            </tbody>
        </table>
    </div>

    <div class="search-container0">
        <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
            <p>Refund Method</p>
            <select class="shipping-input2 clean normal-input same-height-with-date" type="text" id="insert_refund_method" name="insert_refund_method">
                <!-- <option>iPay88</option>
                <option>Online Banking</option> -->
                <option value=" " name=" ">SELECT AN OPTION</option>
                <option value="iPay88" name="iPay88">iPay88</option>
                <option value="Online Banking" name="Online Banking">Online Banking</option>
            </select>
        </div>

        <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
            <p>Refund Amount</p>
            <input class="shipping-input2 clean normal-input same-height-with-date" type="number" placeholder="0.00" id="insert_refund_amount" name="insert_refund_amount">
        </div>     

        <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
            <p>Note</p>
            <input class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Note" id="insert_refund_note" name="insert_refund_note">
        </div> 
   
        <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm  ow-mbtm-reason">
            <p>Choose a Reason</p>
            <!-- <select class="shipping-input2 clean normal-input same-height-with-date">
                <option>Out of our delivery zone</option>
                <option>Running out of stock</option>
            </select> -->
            <select class="shipping-input2 clean normal-input same-height-with-date" type="text" id="insert_refund_reason" name="insert_refund_reason">
                <!-- <option>iPay88</option>
                <option>Online Banking</option> -->
                <option value=" " name=" ">SELECT AN REASON</option>
                <option value="Out Of Our Delivery Zone" name="Out Of Our Delivery Zone">Out of our delivery zone</option>
                <option value="Running Out Of Stock" name="Running Out Of Stock">Running out of stock</option>
            </select>
        </div>                   

        <!-- <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
            <textarea class="shipping-input2 clean normal-input same-height-with-date reason-textarea" placeholder="Or write a reason here"></textarea>
        </div>  -->

        <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="order_id" name="order_id" value="<?php echo $orderArray[0]->getId()?>">
        <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="shipping_status" name="shipping_status" value="REJECT AND REFUND">
        <!-- <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="shipping_method" name="shipping_method" value="-">
        <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="tracking_number" name="tracking_number" value="-"> -->
    </div> 

    <div class="clear"></div>

    <div class="three-btn-container">
        <button input type="submit" name="submit" value="REFUND" class="shipout-btn-a black-button three-btn-a float-left">REFUND</button>
    </div>  

</form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>