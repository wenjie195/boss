<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{


    $icNo = rewrite($_POST["update_icno"]);
    $username = rewrite($_POST["update_username"]);
    $fullname = rewrite($_POST["update_fullname"]);
    $birthday = rewrite($_POST["update_birthday"]);
    $gender = rewrite($_POST["update_gender"]);
    $phoneNo = rewrite($_POST["update_phoneno"]);
    $address = rewrite($_POST["update_address"]);

    $bankName = rewrite($_POST["update_bankname"]);
    $bankAccountHolder = rewrite($_POST["update_bankaccountholder"]);
    $bankAccountNo = rewrite($_POST["update_bankaccountnumber"]);

    $carModel = rewrite($_POST["update_carmodel"]);
    $carYear = rewrite($_POST["update_caryear"]);

    $email = rewrite($_POST["update_email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        echo "email confic";
        //$emailErr = "Invalid email format";
    }
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];
$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/editProfile.php" />
    <meta property="og:title" content="Edit Profile | Boss" />
    <title>Edit Profile | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/editProfile.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
<form method="POST" onsubmit="return editprofileFunc(name);" action="utilities/editProfileFunction.php">

    <div class="edit-profile-div2">
        <h1 class="username"> <?php echo $userDetails->getUsername();?> </h1>
        <!-- <h2 class="profile-title">BASIC INFORMATION</h2> -->
        <h2 class="profile-title"><?php echo _MAINJS_PROFILE_BASIC_INFORMATION ?></h2>
        <table class="edit-profile-table white-text">

        <?php   $fullName = $userDetails->getFullname();
        if ($fullName)
        {
        ?>

        	<tr class="profile-tr">
                <!-- <td class="profile-td1">IC Number</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_IC_NUMBER ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getIcNo();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Username</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_USERNAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getUsername();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Full Name</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_FULLNAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getFullname();?></td>
            </tr>

        <?php
        }
        ?>

        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Birthday</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_BIRTHDAY ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_birthday" class="clean edit-profile-input" type="text" placeholder="YYYY-MM-DD" value="<?php echo $userDetails->getBirthday();?>" name="update_birthday"></td>
            </tr>
        </table>



        <!-- <h2 class="profile-title">BANK INFORMATION</h2> -->
        <h2 class="profile-title"><?php echo _MAINJS_PROFILE_BANK_INFORMATION ?></h2>
        <table class="edit-profile-table white-text">

            <tr class="profile-tr">
                <!-- <td class="profile-td1">Bank</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_BANK ?></td>
                <td class="profile-td2">:</td>


                <td class="profile-td3">
                	<select class="edit-profile-input edit-profile-select clean" id="update_bankname" value="<?php echo $userDetails->getBankName();?>" name="update_bankname">
                        <?php
                        if($userDetails->getBankName() == '')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BHD" name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value=" "  name=" "></option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'AFFIN BANK BERHAD')
                        {
                            ?>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BHD" name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="AFFIN BANK BERHAD"  name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'AL-RAJHI BANKING & INVESTMENT CORP (M) BHD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BHD" name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD"  name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'ALLIANCE BANK MALAYSIA BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BHD" name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="ALLIANCE BANK MALAYSIA BERHAD"  name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'AMBANK BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BHD" name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="AMBANK BERHAD"  name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BANGKOK BANK BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BHD" name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BANGKOK BANK BERHAD"  name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BANK ISLAM MALAYSIA BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BHD" name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BANK ISLAM MALAYSIA BERHAD"  name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BANK KERJASAMA RAKYAT MALAYSIA BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BHD" name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD"  name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BANK MUALAMAT MALAYSIA BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BHD" name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BANK MUALAMAT MALAYSIA BERHAD"  name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BANK OF AMERICA (MALAYSIA) BHD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BERHAD" name="BANK OF CHINA (MALAYSIA) BERHAD">BANK OF CHINA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BANK OF AMERICA (MALAYSIA) BHD"  name="BANK OF AMERICA (MALAYSIA) BHD">BANK OF AMERICA (MALAYSIA) BHD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BANK OF CHINA (MALAYSIA) BHD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BANK OF CHINA (MALAYSIA) BHD"  name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BANK OF TOKYO-MITSUBISHI UFJ (M) BHD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD"  name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BANK PERTANIAN MALAYSIA BHD (AGROBANK)')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)"  name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BANK SIMPANAN NASIONAL BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BANK SIMPANAN NASIONAL BERHAD"  name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'BNP PARIBAS MALAYSIA BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="BNP PARIBAS MALAYSIA BERHAD"  name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'CIMB BANK BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="CIMB BANK BERHAD"  name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'CITIBANK')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="CITIBANK"  name="CITIBANK">CITIBANK</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'DEUTSCHE BANK (MALAYSIA) BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="DEUTSCHE BANK (MALAYSIA) BERHAD"  name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'HONG LEONG BANK BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="HONG LEONG BANK BERHAD"  name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'HSBC BANK MALAYSIA BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="HSBC BANK MALAYSIA BERHAD"  name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'MAYBANK')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="MAYBANK"  name="MAYBANK">MAYBANK</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'MBSB BANK BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="MBSB BANK BERHAD"  name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'PUBLIC BANK BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="RHB BANK BERHAD" name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <option selected value="PUBLIC BANK BERHAD"  name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <?php
                        }

                        else if($userDetails->getBankName() == 'RHB BANK BERHAD')
                        {
                            ?>
                            <option value="AFFIN BANK BERHAD" name="AFFIN BANK BERHAD">AFFIN BANK BERHAD</option>
                            <option value="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD" name="AL-RAJHI BANKING & INVESTMENT CORP (M) BHD">AL-RAJHI BANKING & INVESTMENT CORP (M) BHD</option>
                            <option value="ALLIANCE BANK MALAYSIA BERHAD" name="ALLIANCE BANK MALAYSIA BERHAD">ALLIANCE BANK MALAYSIA BERHAD</option>
                            <option value="AMBANK BERHAD" name="AMBANK BERHAD">AMBANK BERHAD</option>
                            <option value="BANGKOK BANK BERHAD" name="BANGKOK BANK BERHAD">BANGKOK BANK BERHAD</option>
                            <option value="BANK ISLAM MALAYSIA BERHAD" name="BANK ISLAM MALAYSIA BERHAD">BANK ISLAM MALAYSIA BERHAD</option>
                            <option value="BANK KERJASAMA RAKYAT MALAYSIA BERHAD" name="BANK KERJASAMA RAKYAT MALAYSIA BERHAD">BANK KERJASAMA RAKYAT MALAYSIA BERHAD</option>
                            <option value="BANK MUALAMAT MALAYSIA BERHAD" name="BANK MUALAMAT MALAYSIA BERHAD">BANK MUALAMAT MALAYSIA BERHAD</option>
                            <option value="BANK OF AMERICA (MALAYSIA) BERHAD" name="BANK OF AMERICA (MALAYSIA) BERHAD">BANK OF AMERICA (MALAYSIA) BERHAD</option>
                            <option value="BANK OF CHINA (MALAYSIA) BHD" name="BANK OF CHINA (MALAYSIA) BHD">BANK OF CHINA (MALAYSIA) BHD</option>
                            <option value="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD" name="BANK OF TOKYO-MITSUBISHI UFJ (M) BHD">BANK OF TOKYO-MITSUBISHI UFJ (M) BHD</option>
                            <option value="BANK PERTANIAN MALAYSIA BHD (AGROBANK)" name="BANK PERTANIAN MALAYSIA BHD (AGROBANK)">BANK PERTANIAN MALAYSIA BHD (AGROBANK)</option>
                            <option value="BANK SIMPANAN NASIONAL BERHAD" name="BANK SIMPANAN NASIONAL BERHAD">BANK SIMPANAN NASIONAL BERHAD</option>
                            <option value="BNP PARIBAS MALAYSIA BERHAD" name="BNP PARIBAS MALAYSIA BERHAD">BNP PARIBAS MALAYSIA BERHAD</option>
                            <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                            <option value="CITIBANK" name="CITIBANK">CITIBANK</option>
                            <option value="DEUTSCHE BANK (MALAYSIA) BERHAD" name="DEUTSCHE BANK (MALAYSIA) BERHAD">DEUTSCHE BANK (MALAYSIA) BERHAD</option>
                            <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                            <option value="HSBC BANK MALAYSIA BERHAD" name="HSBC BANK MALAYSIA BERHAD">HSBC BANK MALAYSIA BERHAD</option>
                            <option value="MAYBANK" name="MAYBANK">MAYBANK</option>
                            <option value="MBSB BANK BERHAD" name="MBSB BANK BERHAD">MBSB BANK BERHAD</option>
                            <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                            <option selected value="RHB BANK BERHAD"  name="RHB BANK BERHAD">RHB BANK BERHAD</option>
                            <?php
                        }
                        ?>
                    </select><img src="img/dropdown2.png" class="dropdown-png">
                </td>
            </tr>

        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Acc. Name</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_ACCOUNT_HOLDER_NAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $fullName ?></td>
                <input type="hidden" id="update_bankaccountholder" class="clean edit-profile-input white-text"  value="<?php echo $fullName ?>" name="update_bankaccountholder">
            </tr>
            <tr class="profile-tr">
                <!-- <td class="profile-td1">Acc. No.</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_ACCOUNT_NUMBER ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_bankaccountnumber" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getBankAccountNo();?>" name="update_bankaccountnumber"></td>
            </tr>
        </table>

        <!-- <h2 class="profile-title">CONTACT INFORMATION</h2> -->
        <h2 class="profile-title"><?php echo _MAINJS_PROFILE_CONTACT_INFORMATION ?></h2>
        <table class="edit-profile-table white-text">
            <tr class="profile-tr">
                <!-- <td class="profile-td1">Email</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_EMAIL ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input id="update_email" class="clean edit-profile-input" type="email" value="<?php echo $userDetails->getEmail();?>" name="update_email">
                </td>
            </tr>


            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Phone</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_PHONE ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_phoneno" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getPhoneNo();?>" name="update_phoneno" required></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Address</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_ADDRESS ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_address" class="clean edit-profile-input" type="text" placeholder="" value="<?php echo $userDetails->getAddress();?>" name="update_address"></td>
            </tr>

        </table>
        <button input type="submit" name="submit" value="Submit" class="confirm-btn text-center white-text clean black-button"><?php echo _MAINJS_EDIT_PROFILE_CONFIRM ?></button>
        <p class="change-password-p"><a href="editPassword.php" class="edit-password-a black-link"><?php echo _MAINJS_EDIT_PROFILE_EDIT_PASSWORD ?></a></p>
    </div>
</form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Error";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Data.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Data Update Successfully.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
