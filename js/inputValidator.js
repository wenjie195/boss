function validateMyForm(msgArray)
{
    let errorArray = [];
    let canValidateLevel_2 = true;
    let combinationArray =[errorArray,canValidateLevel_2];

    let email = $('#email').val();
    let country = $('#country_code').val();
    let phone = $('#phone_number').val();
    let password = $('#password').val();
    let confirmPassword = $('#confirm_password').val();
    let ic_no = $('#ic_no').val();

    if(password.length < 6)
    {
        combinationArray[0].push(msgArray["_MAINJS_VALID_PASSWORD_LENGTH"]);
        combinationArray[1] = false;
    }

    if(!isValidEmailAddress(email))
    {
        combinationArray[0].push(msgArray["_MAINJS_VALID_EMAIL_ADDRESS"]);
        combinationArray[1] = false;
    }

    combinationArray = checkNullOrEmpty(combinationArray,email,msgArray["_MAINJS_ENTER_EMAIL"]);
    combinationArray = checkNullOrEmpty(combinationArray,country,msgArray["_MAINJS_SELECT_COUNTRY"]);
    combinationArray = checkNullOrEmpty(combinationArray,phone,msgArray["_MAINJS_ENTER_PHONENO"]);
    combinationArray = checkNullOrEmpty(combinationArray,password,msgArray["_MAINJS_ENTER_PASSWORD"]);
    combinationArray = checkNullOrEmpty(combinationArray,confirmPassword,msgArray["_MAINJS_ENTER_CONFIRM_PASSWORD"]);
    combinationArray = checkNullOrEmpty(combinationArray,ic_no,msgArray["_MAINJS_ENTER_ICNO"]);

    if($('#check_tnc').is(':checked') === false)
    {
        combinationArray[0].push(msgArray["_MAINJS_ACCEPT_TERMS"]);
        combinationArray[1] = false;
    }
    if(!combinationArray[1])
    {
        let errorText = msgArray["_MAINJS_ENTER_BELOW_INFO"] + " ：<br>";
        for(let counter = 0;counter < combinationArray[0].length ;counter++)
        {
            errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
        }

        putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",errorText);
        event.preventDefault();
    }
    else
    {
        if(country != null && country !="")
        {
            if(country === 86 || country === "86") //China
            {
                if(!validid.cnid(ic_no))
                {
                    putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                    event.preventDefault();
                }
            }
            else if(country === 886 || country === "886") //Taiwan
            {
                if(!validid.twid(ic_no) && !validid.twrc(ic_no))
                {
                    putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                    event.preventDefault();
                }
            }
            else if(country === 852 || country === "852") //Hong Kong
            {
                if(!validid.hkid(ic_no))
                {
                    putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                    event.preventDefault();
                }
            }
            else if(country === 65 || country === "65") //Singapore ->LINK : https://stackoverflow.com/questions/29743154/regular-expression-for-nric-fin-in-singapore
            {
                if(!ic_no.match(/^[STFG]\d{7}[A-Z]$/))
                {
                    putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                    event.preventDefault();
                }
            }
            else if(country === 60 || country === "60") //Malaysia
            {
                if(!ic_no.match(/^([0-9][0-9])((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))([0-9][0-9])([0-9][0-9][0-9][0-9])$/))
                {
                    putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                    event.preventDefault();
                }
            }
        }
        else
        {
            putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_COUNTRY_NULL_"]);
            event.preventDefault();
        }
    }
}

function checkNullOrEmpty(combinationArray,field,errTXT)
{
    if(field == null || field == "" || field.length == 0)
    {
        combinationArray[0].push(errTXT);
        combinationArray[1] = false;
    }
    return combinationArray;
}