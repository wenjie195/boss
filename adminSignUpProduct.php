<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/SignUpProduct.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

// $memberList = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"i");

$userSignUpProduct = getSignUpProduct($conn);

$aaa = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Oil Booster 3pcs (1 set) RM300'),"s");
$bbb = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Motorcycle Oil Booster 15pcs (1 set) RM300'),"s");
$ccc = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Synthetic Plus Engine Oil 5w-30 (1 set) RM300'),"s");
$ddd = getSignUpProduct($conn," WHERE product = ? ",array("product"),array('Synthetic Plus Engine Oil 10w-40  (1 set) RM300'),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/adminSignUpProduct.php" />
    <meta property="og:title" content="Admin Sign Up Product | Boss" />
    <title>Admin Sign Up Product | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/adminSignUpProduct.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="details-h1 gold-text">User Sign Up Product</h1>

    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest</option>
    	<option class="filter-option">Oldest</option>
    </select> -->
    <!-- End of Filter -->

    <!-- <div class="clear"></div> -->

	<!-- <div class="search-container0">

            <div class="shipping-input clean smaller-text2">
                <p>Name</p>
                <input class="shipping-input2 clean normal-input" type="text" placeholder="Name">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>Contact</p>
                <input class="shipping-input2 clean normal-input" type="number" placeholder="Contact">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Email</p>
                <input class="shipping-input2 clean normal-input" type="email" placeholder="Email">
            </div>
            <div class="shipping-input clean smaller-text2 second-shipping">
                <p>Start Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="Start Date">
            </div>


            <div class="shipping-input clean middle-shipping-div smaller-text2">
                <p>End Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="End Date">
            </div>

            <button class="clean black-button shipping-search-btn second-shipping">Search</button>

    </div>     -->

    <?php
    if($aaa)
        {   //echo count($aaa);
            $aaaa = count($aaa);
        }else {
          $aaaa = 0;
        }
    ?>
    <?php
    if($bbb)
        {   //echo count($bbb);
            $bbbb = count($bbb);

        }else {
          $bbbb = 0;
        }
    ?>
    <?php
    if($ccc)
        {   //echo count($ccc);
            $cccc = count($ccc);

        }else {
          $cccc = 0;
        }
    ?>
    <?php
    if($ddd)
        {   //echo count($ddd);
            $dddd = count($ddd);

        }else {
          $dddd = 0;
        }
    ?>

    <div class="our-div-container">
            <div class="four-white-div hover1 white-div-yellow normal-pointer">
                <img src="img/oil-booster-a1.png" class="four-img hover1a" alt="Oil Booster" title="Oil Booster">
                <img src="img/oil-booster-a2.png" class="four-img hover1b" alt="Oil Booster" title="Oil Booster">
                <p class="four-div-p four-p-mh"><b>Oil Booster 3pcs</b><br>(1 set)</p>
                <p class="four-div-p"><span class="bigger-amount"><?php echo $aaaa;?></span></p>
            </div>

            <div class="four-white-div hover1 four-middle-div1 white-div-yellow normal-pointer">
                <img src="img/motor-oil1.png" class="four-img hover1a" alt="Motorcycle Oil Booster" title="Motorcycle Oil Booster">
                <img src="img/motor-oil2.png" class="four-img hover1b" alt="Motorcycle Oil Booster" title="Motorcycle Oil Booster">
            	<p class="four-div-p four-p-mh"><b>Motorcycle Oil Booster 15pcs</b><br>(1 set)</p>
                <p class="four-div-p"><span class="bigger-amount"><?php echo $bbbb;?></span></p>
            </div>

            <div class="four-white-div hover1 four-middle-div2 white-div-yellow normal-pointer">
                <img src="img/product1.png" class="four-img hover1a" alt="Synthetic Plus Engine Oil 5w-30" title="Synthetic Plus Engine Oil 5w-30">
                <img src="img/product2.png" class="four-img hover1b" alt="Synthetic Plus Engine Oil 5w-30" title="Synthetic Plus Engine Oil 5w-30">
            	<p class="four-div-p four-p-mh"><b>Synthetic Plus Engine Oil 5w-30</b><br>(1 set)</p>
                <p class="four-div-p"><span class="bigger-amount"><?php echo $cccc;?></span></p>
            </div>

            <div class="four-white-div hover1 white-div-yellow normal-pointer">
                <img src="img/engine-oil1.png" class="four-img hover1a" alt="Synthetic Plus Engine Oil 10w-40" title="Synthetic Plus Engine Oil 10w-40">
                <img src="img/engine-oil2.png" class="four-img hover1b" alt="Synthetic Plus Engine Oil 10w-40" title="Synthetic Plus Engine Oil 10w-40">
            	<p class="four-div-p four-p-mh"><b>Synthetic Plus Engine Oil 10w-40</b><br>(1 set)</p>
            	<p class="four-div-p"><span class="bigger-amount"><?php echo $dddd;?></span></p>

            </div>

    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>NAME</th>
                        <th>PRODUCT</th>
                        <th>QUANTITY</th>
                        <th>SIGN UP BY</th>
                        <th>DATE</th>
                        <th>DETAILS</th>
                    </tr>
                </thead>

                <tbody>

                <?php
                if($userSignUpProduct)
                {
                    // echo count($userSignUpProduct);
                    for($cnt = 0;$cnt < count($userSignUpProduct) ;$cnt++)
                    {?>
                        <tr>
                            <!-- <td><?php //echo ($cnt+1)?></td> -->
                            <!-- <td><?php //echo $userSignUpProduct[$cnt]->getReferralUid();?></td> -->
                            <td><?php echo $userSignUpProduct[$cnt]->getId();?></td>
                            <td><?php echo $userSignUpProduct[$cnt]->getReferralName();?></td>
                            <td><?php echo $userSignUpProduct[$cnt]->getProduct();?></td>
                            <td><?php echo $userSignUpProduct[$cnt]->getQuantity();?></td>
                            <td><?php echo $userSignUpProduct[$cnt]->getReferrerName();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($userSignUpProduct[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>

                            <td>
                                <form action="adminSignUpProductDetails.php" method="POST">
                                    <button class="clean edit-anc-btn hover1" type="submit" name="signupproduct_id" value="<?php echo $userSignUpProduct[$cnt]->getId();?>">
                                        <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Shipping Out" title="Details">
                                        <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Shipping Out" title="Details">
                                    </button>
                                </form>
                            </td>

                    <?php
                    }?>
                        </tr>
                        <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
function goBack()
{
  window.history.back();
}
</script>

</body>
</html>
