<style>
.imagetj
{
  opacity: 1;
  display: block;
  width: 98%;
  height:98%;
  transition: .5s ease;
  backface-visibility: hidden;
}
.edit-profile-div-smalltj
{
    margin-left: auto;
    margin-right: auto;
    width: 200px;
	height:200px;
}
</style>

<div class="left-profile-div"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": ".9"}'>
        <?php
        if($pictures == "")
        {
        ?>
            <div class="profile-div">
                <a href="uploadProfilePicture.php">
                    <button class="edit-profile-pic-btn text-center white-text clean"><img src="img/camera.png" class="camera-icon" alt="Update Profile Picture" title="Update Profile Picture"><?php echo _MAINJS_PROFILE_UPDATE ?></button>
                </a>
            </div>
            <p class="edit-profile-p text-center width100 hover1">
                <a href="editProfile.php" class="profile-a"><img src="img/edit2.png" class="edit-profile-icon hover1a" alt="Edit Profile" title="Edit Profile"><img src="img/edit3.png" class="edit-profile-icon hover1b" alt="Edit Profile" title="Edit Profile"><?php echo _MAINJS_PROFILE_EDIT_PROFILE ?></a>
            </p>
        <?php
        }
        else
        { ?>
           <div class="edit-profile-div-smalltj text-center white-text clean">
             <img src="upload/<?php echo $pictures?>"  style="width:100%" class="imagetj camera-icon overwrite-imagetj" alt="Update Profile Picture" title="Update Profile Picture">
                <a href="uploadProfilePicture.php">
                	<button class="edit-profile-pic-btn text-center white-text clean ow-update-pic-btn"><img src="img/camera.png" class="camera-icon" alt="Update Profile Picture" title="Update Profile Picture"><?php echo _MAINJS_PROFILE_UPDATE ?></button>
                </a>
                <p class="edit-profile-p text-center width100">
                    <a href="editProfile.php" class="profile-a"><img src="img/edit2.png" class="edit-profile-icon" alt="Edit Profile" title="Edit Profile"> Edit Profile</a>
                </p>
            </div>

        <?php
        }
        ?>
</div>
