<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$productsOrders =  getProductOrders($conn);

$post = array();
$start_date = date("Y-m-d");
$end_date = date("Y-m-d");
$username = "";
$phoneNo="";
$trackingNumber="";

$record_per_page = 15;
$page = '';
if(isset($_GET["page"]))
{
 $page = $_GET["page"];
}
else
{
 $page = 1;
}

$start_from = ($page-1)*$record_per_page;

if (isset($_GET["start_date"]) && isset($_GET["end_date"]) && isset($_GET["owner"]))
{
	$start_date = $_GET["start_date"];
	$end_date = $_GET["end_date"];
	$username = $_GET["owner"];
  $withdrawalNumber = $_GET["withdrawal_number"];
  $phoneNo = $_GET["contact"];
  //$trackingNumber = $_GET["tracking_number"];
  //$phoneNo = $_GET["contact"];
	$post = $_GET;
}

$products = getProduct($conn);
$list = GetList($post, $conn,$start_from,$record_per_page);

//$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function GetList($post, $conn,$start_from,$record_per_page)
{
	$sql = "SELECT owner,date_created,bank_name,withdrawal_status, ";
	$sql .= "withdrawal_number,contact,amount ";
	$sql .= "FROM withdrawal ";
	$sql .= "WHERE withdrawal_status = 'PENDING' ";

  if (isset($post['reset'])) {
    if (isset($post["start_date"]) && strlen($post["start_date"]) < 0)
  	{
  		$sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
  	}

  	if (isset($post["end_date"]) && strlen($post["end_date"]) < 0)
  	{
  		$sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
  	}

  	if (isset($post["owner"]) && strlen($post["owner"]) < 0)
  	{
      $sql .= "AND owner LIKE '%" . $post["owner"] . "%' ";
  	}
    if (isset($post["withdrawal_number"]) && strlen($post["withdrawal_number"]) < 0)
  	{
      $sql .= "AND withdrawal_number LIKE '%" . $post["withdrawal_number"] . "%' ";
  	}
    if (isset($post["contact"]) && strlen($post["contact"]) < 0)
  	{
      $sql .= "AND contact LIKE '%" . $post["contact"] . "%' ";
  	}
    if (isset($post["bank_name"]) && strlen($post["bank_name"]) < 0)
  	{
      $sql .= "AND bank_name LIKE '%" . $post["bank_name"] . "%' ";
  	}
  }else {
    if (isset($post["start_date"]))
  	{
  		$sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
  	}

  	if (isset($post["end_date"]))
  	{
  		$sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
  	}

  	if (isset($post["owner"]) && strlen($post["owner"]) > 0)
  	{
      $sql .= "AND owner LIKE '%" . $post["owner"] . "%' ";
  	}
    if (isset($post["withdrawal_number"]) && strlen($post["withdrawal_number"]) > 0)
  	{
      $sql .= "AND withdrawal_number LIKE '%" . $post["withdrawal_number"] . "%' ";
  	}
    if (isset($post["contact"]) && strlen($post["contact"]) > 0)
  	{
      $sql .= "AND contact LIKE '%" . $post["contact"] . "%' ";
  	}
    if (isset($post["bank_name"]) && strlen($post["bank_name"]) > 0)
  	{
      $sql .= "AND bank_name LIKE '%" . $post["bank_name"] . "%' ";
  	}
  }


	$sql .= "ORDER BY date_created ASC LIMIT $start_from, $record_per_page";
	//echo $sql;exit;

	$result = $conn->query($sql);
	$output = array();

	if ($result->num_rows > 0)
	{
		// output data of each row
		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}
	}

	return $output;

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/adminWithdrawal.php" />
    <meta property="og:title" content="Withdrawal Request | Boss" />
    <title>Withdrawal Request | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/adminWithdrawal.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

  <!-- <h1 class="h1-title h1-before-border shipping-h1">Withdrawal Request | <a href="adminWithdrawalComp.php" class="white-text title-tab-a">Completed</a></h1> -->
  <h1 class="h1-title h1-before-border shipping-h1">
    <?php echo _MAINJS_ADMWD_WITHDRAWAL_REQUEST ?> | <a href="adminWithdrawalComp.php" class="white-text title-tab-a"><?php echo _MAINJS_ADMWD_COMPLETE ?></a>
    | <a href="adminWithdrawalReject.php" class="white-text title-tab-a"><?php echo _MAINJS_ADMWD_REJECTED ?></a>
  </h1>
  <!-- This is a filter for the table result -->

    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->

    <div class="clear"></div>

    <?php
    // if($productsOrders == "")
    // {
    ?>
        <!-- <h3 class="profile-title">SHIPPING ORDER IS EMPTY</h3> -->
        <!-- <h3 class="profile-title">Shipping order is empty</h3> -->
    <?php
    // }
    // else
    // { ?>

	<div class="search-container0 payout-search">
    <form action="adminWithdrawal.php" type="post">
            <div class="shipping-input clean smaller-text2 ow-shipping-input-with">
                <!-- <p>Withdrawal No.</p> -->
                <p class="white-text"><?php echo _MAINJS_ADMWD_WITHDRAWAL_NO ?></p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="number" name="withdrawal_number" placeholder="Withdrawal Number"  value="<?php echo $withdrawalNumber; ?>">
            </div>
            <div class="shipping-input clean smaller-text2 ow-shipping-input-with">
                <!-- <p>Username</p> -->
                <p class="white-text"><?php echo _MAINJS_ADMWD_USERNAME ?></p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" name="owner" placeholder="Username"  value="<?php echo $username; ?>">
            </div>
            <!-- <div class="shipping-input clean smaller-text2">
                <p>Tracking No.</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" name="tracking_number" placeholder="Tracking Number"  value="<?php echo $trackingNumber; ?>">
            </div> -->
            <!-- <div class="shipping-input clean smaller-text2">
                <p>Contact</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="number" name="contact" placeholder="Contact"  value="<?php echo $phoneNo; ?>">
            </div> -->
            <div class="shipping-input clean smaller-text2 ow-shipping-input-with">
                <!-- <p>Start Date</p> -->
                <p class="white-text"><?php echo _MAINJS_ADMWD_START_DATE ?></p>
                <input class="shipping-input2 clean normal-input" name="start_date" type="date" value="<?php echo $start_date; ?>">
            </div>
            <div class="shipping-input clean smaller-text2 ow-shipping-input-with">
                <!-- <p>End Date</p> -->
                <p class="white-text"><?php echo _MAINJS_ADMWD_END_DATE ?></p>
                <input class="shipping-input2 clean normal-input" name="end_date" type="date" value="<?php echo $end_date; ?>">
            </div>
            <button type="submit" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 six-search ow-with-btn ow-with-btn2"><?php echo _MAINJS_ADMWD_SEARCH ?></button>
            <button type="submit" name="reset" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 six-search ow-with-btn"><?php echo _MAINJS_ADMWD_RESET ?></button>
            <!-- <button type="submit" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 six-search ow-with-btn ow-with-btn2">Search</button>
            <button type="submit" name="reset" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 six-search ow-with-btn">Reset</button> -->
      </form>
  </div>
    <div class="clear"></div>

    <div class="width100 shipping-div2 scroll-div">
        <?php

         if (!$list) {
          ?><center>  <div class= "width100 oveflow">
              <div class="width20">
                  <div class="white50div">
              <?php echo "*There is No Withdrawal Request For Now." ?>
            </div>
        </div>
        </div></center><?php
      }else {



        $conn = connDB();?>
            <!-- <table class="shipping-table">     -->
            <table class="shipping-table scroll-table">
                <thead>
                  <tr>
                      <!-- <th>NO.</th> -->
                      <th><?php echo _MAINJS_ADMWD_WITHDRAWAL_NO_2 ?></th>
                      <th><?php echo _MAINJS_ADMWD_USERNAME_2 ?></th>
                      <!-- <th>CONTACT</th> -->
                      <!-- <th>BANK</th> -->
                      <th><?php echo _MAINJS_ADMWD_AMOUNT ?></th>
                      <th><?php echo _MAINJS_ADMWD_BANK ?></th>
                      <th><?php echo _MAINJS_ADMWD_REQUEST_DATE ?></th>
                      <th><?php echo _MAINJS_ADMWD_ACTION ?></th>
                      
                  </tr>

                  <!-- <tr>
                      <th>WITHDRAWAL NO.</th>
                      <th>USERNAME</th>
                      <th>AMOUNT (RM)</th>
                      <th>BANK</th>
                      <th>REQUEST DATE</th>
                      <th>ACTION</th>
                  </tr> -->

                </thead>
                <tbody>
								<?php if ($list):
                  $ind=0;?>
									<?php foreach ($list AS $ls):
                    $ind++; ?>
										<tr>
											<!-- <td><?php //echo $ind; ?></td> -->
											<td>#<?php echo $ls["withdrawal_number"]; ?></td>
                      <td><?php echo $ls["owner"]; ?></td>
                      <!-- <td><?php //echo $ls["contact"]; ?></td> -->
                      <td><?php echo $ls["amount"]; ?></td>
                      <td><?php echo $ls["bank_name"]; ?></td>
                      <!-- <td><?php// echo $ls["date_created"]; ?></td> -->
                      <td><?php echo date("d-m-Y", strtotime($ls["date_created"])); ?></td>
                      <td>
                          <form action="withdrawalReq.php" method="POST">
                              <button class="clean edit-anc-btn hover1" type="submit" name="withdrawal_number" value="<?php echo $ls["withdrawal_number"];?>">
                                  <!-- <img src="img/shipping1.png" class="edit-announcement-img hover1a" alt="Delivery" title="Delivery">
                                  <img src="img/shipping2.png" class="edit-announcement-img hover1b" alt="Delivery" title="Delivery"> -->
                                  <img src="img/edit2.png" class="edit-announcement-img hover1a" alt="Shipping Out" title="Review Shipping">
                                  <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Shipping Out" title="Review Shipping">
                              </button>
                          </form>

                      </td>
										</tr>
									<?php endforeach; ?>
								<?php else: ?>
									<tr>
										<!-- <td colspan="7">No result</td> -->
								</tr>
								<?php endif; ?>
                  </tbody>
            </table><br>
        <?php //$conn->close();?>
    </div>
	<div class="clear"></div>
		<div class="pagination">
			<div class="width100 text-center">
            	<ul>
		<?php
		$page_query = "SELECT * FROM withdrawal WHERE `withdrawal_status` ='PENDING' ORDER BY date_created ASC";
		$page_result = mysqli_query($conn, $page_query);
		$total_records = mysqli_num_rows($page_result);
		$total_pages = ceil($total_records/$record_per_page);
		$start_loop = $page;
		$difference = $total_pages - $page;
		if($difference <= 5)
		{
		 $start_loop = $total_pages - 5;
		}
		$end_loop = $start_loop + 4;
		if($page > 1)
		{
		 echo "<a href='adminWithdrawal.php?page=1'><li>First</li></a>";
		 echo "<a href='adminWithdrawal.php?page=".($page - 1)."'><li><<</li></a>";
		}
		for($i=1; $i<=$total_pages; $i++)
		{
				echo "<a href='adminWithdrawal.php?page=".$i."'><li>".$i."</li></a>";
		}
		if($page <= $end_loop)
		{
		 echo "<a href='adminWithdrawal.php?page=".($page + 1)."'><li>>></li></a>";
		 echo "<a href='adminWithdrawal.php?page=".$total_pages."'><li>Last</li></a>";
		}


		?>
        	</ul>
		</div>
		</div>
          </div>

        <?php }//$conn->close();?>
    </div>

    <?php
    //}?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<style>
.pagination {
  	display: inline-block;
	text-align: center;
	width:100%;
}

.pagination li {
  color: black;
  padding: 8px 16px;
  text-decoration: none;
	text-align: center;
  transition:ease-in-out 0.15s;;
  border: 1px solid white;
  margin: 0 4px;
	background-color: white;
	display:inline-block;
}
.pagination a {
	display:inline-block;
	margin-bottom:10px;
}
.pagination li.active {
  background-color: #ddb432;
  color: white;
  border: 1px solid #ddb432;
}

.pagination li:hover:not(.active) {
	background-color: #ddb432;
  	color: white;
  	border: 1px solid #ddb432;}
</style>
</body>
</html>
