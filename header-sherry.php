   
    <header id="header" class="header header--fixed header1 menu-color" role="banner">
        <div class="width100 gold-line same-padding"></div>
        <div class="big-container-size hidden-padding same-padding" id="top-menu">
        	
            <div class="left-logo-div float-left hidden-logo-padding">
                <a href="index.php"><img src="img/boss.png" class="logo-img" alt="Boss" title="Boss"></a>
            </div>
            <?php

            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
                $_SERVER['REQUEST_URI'];

            if(isset($_SESSION['usertype_level']) && $_SESSION['usertype_level'] == 1)
            {
                ?>
                 <div class="middle-menu-div float-left hide">
                    <a href="index.php#about" class="menu-padding white-to-yellow">About</a>
                    <a href="index.php#products" class="menu-padding white-to-yellow">Products</a>
                    <a href="index.php#contact" class="menu-padding white-to-yellow">Contact Us</a>
                    <a href="faq.php" class="menu-padding white-to-yellow">FAQ</a>
                </div>

                <?php
                if(isset($_SESSION['uid']))
                {
                    // echo count($_SESSION);
                    ?>
                    <div class="right-menu-div float-right" id="after-login-menu">
                        <!-- <a href="announcement.php"  class="menu-padding"><img src="img/notifcation.png" class="cart-img user-notification" alt="Notification" title="Notification"><div class="dot-div hide"></div></a> -->
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/white-user.png" class="cart-img" alt="profile" title="profile"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <!-- <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a"><img src="img/user-black.png" class="cart-img dropdown-img2" alt="Profile" title="Profile"> My Profile</a></p>
                                <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a"><img src="img/edit-profile.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> Edit Profile</a></p>
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a"><img src="img/password-d.png" class="cart-img dropdown-img2" alt="Password" title="Password"> Edit Password</a></p>
                                <p class="dropdown-p"><a href="addePin.php"  class="menu-padding dropdown-a"><img src="img/e-pin.png" class="cart-img dropdown-img2" alt="E-Pin" title="E-Pin"> E-Pin</a></p>
                                <p class="dropdown-p"><a href="referee.php" class="menu-padding dropdown-a"><img src="img/user-group.png" class="cart-img dropdown-img2" alt="My Referee" title="My Referee"> My Referee</a></p>
                                <p class="dropdown-p"><a href="addReferee.php" class="menu-padding dropdown-a"><img src="img/add-referee.png" class="cart-img dropdown-img2" alt="Add Referee" title="Add Referee"> Add Referee</a></p>
                            </div> -->

                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a"><img src="img/user-black.png" class="cart-img dropdown-img2" alt="Profile" title="Profile"> <?php echo _MAINJS_HEADER_MY_PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a"><img src="img/edit-profile.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> <?php echo _MAINJS_HEADER_EDIT_PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a"><img src="img/password-d.png" class="cart-img dropdown-img2" alt="Password" title="Password"> <?php echo _MAINJS_HEADER_EDIT_PASSWORD ?></a></p>
                                <p class="dropdown-p"><a href="addePin.php"  class="menu-padding dropdown-a"><img src="img/e-pin.png" class="cart-img dropdown-img2" alt="E-Pin" title="E-Pin"> E-Pin</a></p>
                                <p class="dropdown-p"><a href="referee.php" class="menu-padding dropdown-a"><img src="img/user-group.png" class="cart-img dropdown-img2" alt="My Referee" title="My Referee"> <?php echo _MAINJS_HEADER_MY_REFEREE ?></a></p>
                                <p class="dropdown-p"><a href="addReferee.php" class="menu-padding dropdown-a"><img src="img/add-referee.png" class="cart-img dropdown-img2" alt="Add Referee" title="Add Referee"> <?php echo _MAINJS_HEADER_ADD_REFEREE ?></a></p>
                            </div>

                        </div>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/cart.png" class="cart-img" alt="Purchase" title="Purchase"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <!-- <p class="dropdown-p"><a href="firstPurchasing.php"  class="menu-padding dropdown-a"><img src="img/verify-payment.png" class="cart-img dropdown-img2" alt="Sign Up Product History" title="Sign Up Product History"> <?php //echo _MAINJS_HEADER_SIGN_UP_PRO_HISTORY ?></a></p> -->
                                <p class="dropdown-p"><a href="product.php"  class="menu-padding dropdown-a"><img src="img/bottle.png" class="cart-img dropdown-img2" alt="Purchase Product" title="Purchase Product"> <?php echo _MAINJS_HEADER_PURCHASE_PRO ?></a></p>
                                <p class="dropdown-p"><a href="purchaseHistory.php"  class="menu-padding dropdown-a"><img src="img/verify-payment.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> <?php echo _MAINJS_HEADER_PURCHASE_HIS ?></a></p>
                            </div>
                        </div>    
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/document.png" class="cart-img" alt="Report" title="Report"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>

                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><b><?php echo _MAINJS_HEADER_BONUS_RP ?></b></p>
                                <!-- <p class="dropdown-p"><b>Bonus Report</b></p> -->
                                <p class="dropdown-p"><a href="directSponsorBonusReport.php"  class="menu-padding dropdown-a"><img src="img/bonus.png" class="cart-img dropdown-img2" alt="Direct Sponsor" title="Direct Sponsor"> <?php echo _MAINJS_HEADER_DIRECT_SPONSOR ?></a></p>
                                <p class="dropdown-p"><a href="overridingSponsorBonusReport.php"  class="menu-padding dropdown-a"><img src="img/bonus.png" class="cart-img dropdown-img2" alt="Leadership, Same Level" title="Overriding, Same Level Bonus"> <?php echo _MAINJS_HEADER_OVER_SAME ?></a></p>
								<!-- <p class="dropdown-p"><b> </b></p> -->
                            </div>

                        </div>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/language-white.png" class="cart-img" alt="Change Language" title="Change Language"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a"><img src="img/english.png" class="cart-img dropdown-img2" alt="English" title="English"> English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a"><img src="img/chinese.png" class="cart-img dropdown-img2" alt="中文" title="中文"> 中文</a></p>
                            </div>
                        </div>
                        <a href="logout.php"  class="menu-padding"><img src="img/logout-white.png" class="cart-img" alt="Logout" title="Logout"></a>

					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
								  <!-- <li><a href="announcement.php">Announcement</a></li> -->
                                  <!-- <li class="title-li">Profile</li> -->
                                  <li class="title-li"><?php echo _MAINJS_HEADER_PROFILE ?></li>
                                  <li><a href="profile.php" class="mini-li"><?php echo _MAINJS_HEADER_MY_PROFILE ?></a></li>
                                  <li><a href="editProfile.php"  class="mini-li"><?php echo _MAINJS_HEADER_EDIT_PROFILE ?></a></li>
                                  <li><a href="editPassword.php"  class="mini-li"><?php echo _MAINJS_HEADER_EDIT_PASSWORD ?></a></li>
                                  <!-- <li><a href="addePin.php" class="mini-li">E-Pin</a></li> -->

                                  <!-- <li class="title-li">My Team</li>            -->
                                  <li class="title-li"><?php echo _MAINJS_HEADER_MY_TEAM ?></li>                          
                                  <li><a href="referee.php" class="mini-li"><?php echo _MAINJS_HEADER_MY_REFEREE ?></a></li>
                                  <li><a href="addReferee.php" class="mini-li"><?php echo _MAINJS_HEADER_ADD_REFEREE ?></a></li>

                                  <!-- <li class="title-li">Purchase</li> -->
                                  <li class="title-li"><?php echo _MAINJS_HEADER_PURCHASE ?></li>
                                  <!-- <li><a href="firstPurchasing.php" class="mini-li"><?php //echo _MAINJS_HEADER_SIGN_UP_PRO_HISTORY ?></a></li> -->
                                  <li><a href="product.php" class="mini-li"><?php echo _MAINJS_HEADER_PURCHASE_PRO ?></a></li>
                                  <li><a href="purchaseHistory.php" class="mini-li"><?php echo _MAINJS_HEADER_PURCHASE_HIS ?></a></li>
                                  
                                  <li class="title-li"><?php echo _MAINJS_HEADER_BONUS_RP ?></li>
                                  <li><a href="directSponsorBonusReport.php" class="mini-li"><?php echo _MAINJS_HEADER_DIRECT_SPONSOR ?></a></li>
                                  <li><a href="overridingSponsorBonusReport.php" class="mini-li"><?php echo _MAINJS_HEADER_OVER_SAME ?></a></li>
                                  
                                  <!-- <li class="title-li">Language</li> -->
                                  <li class="title-li"><?php echo _MAINJS_HEADER_ADMIN_LANGUAGE ?></li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a>
                                  <a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>

                                  <li  class="last-li"><a href="logout.php"><?php echo _MAINJS_HEADER_LOGOUT ?></a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->

                    </div>
                    <?php
                }
                else
                {
                    ?>
                    <div class="right-menu-div float-right">
                    	<a href="faq.php" class="menu-padding white-to-yellow faq-padding">FAQ</a>
                        <a  class="menu-padding white-to-yellow div-div open-login">Login</a>
                        <a class="open-menu"><img src="img/white-menu.png" class="menu-icon"></a>
                    </div>
                    <?php
                }
            }
            else
            {
                if(isset($_SESSION['uid']))
                {
                ?>
                <div class="right-menu-div1">

                    <!-- <a href="adminDashboard.php" class="menu-padding white-to-yellow">Dashboard</a>
                    <a href="adminPayment.php" class="menu-padding white-to-yellow">Payment</a>
                    <a href="adminShipping.php" class="menu-padding white-to-yellow">Shipping</a>
                    <a href="adminWithdrawal.php" class="menu-padding white-to-yellow">Withdrawal</a>
                    <a href="adminSales.php" class="menu-padding white-to-yellow">Sales</a>
                    <a href="adminProduct.php" class="menu-padding white-to-yellow">Product</a>
                    <a href="adminPayout.php" class="menu-padding white-to-yellow">Payout</a>
                    <a href="adminMember.php" class="menu-padding white-to-yellow">Member</a>
                    <a href="adminAdmin.php" class="menu-padding white-to-yellow">Admin</a> -->

                    <a href="adminDashboard.php" class="gold-a menu-padding menu-a-padding"><?php echo _MAINJS_HEADER_ADMIN_DASHBOARD ?></a>
                    <a href="adminPayment.php" class="gold-a menu-padding menu-a-padding"><?php echo _MAINJS_HEADER_ADMIN_PAYMENT ?></a>
                    <a href="adminShipping.php" class="gold-a menu-padding menu-a-padding"><?php echo _MAINJS_HEADER_ADMIN_SHIPPING ?></a>
                    <a href="adminWithdrawal.php" class="gold-a menu-padding menu-a-padding"><?php echo _MAINJS_HEADER_ADMIN_WITHDRAWAL ?></a>
                    <a href="adminProduct.php" class="gold-a menu-padding menu-a-padding"><?php echo _MAINJS_HEADER_ADMIN_PRODUCT ?></a>
                    <a href="adminMember.php" class="gold-a menu-padding menu-a-padding"><?php echo _MAINJS_HEADER_ADMIN_MEMBER ?></a>

                    <div class="dropdown hover1">
                        <a  class="menu-padding"><img src="img/language-white.png" class="cart-img" alt="Change Language" title="Change Language"></a>
                        <a  class="menu-padding dropdown-btn">
                            <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                            <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                        </a>
                        <div class="dropdown-content yellow-dropdown-content">
                            <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a"><img src="img/english.png" class="cart-img dropdown-img2" alt="English" title="English"> English</a></p>
                            <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a"><img src="img/chinese.png" class="cart-img dropdown-img2" alt="中文" title="中文"> 中文</a></p>
                        </div>
                    </div>
                    <div class="dropdown hover1">
                            <a  class="menu-padding">
                                <img src="img/settings.png" class="cart-img hover1a" alt="Settings" title="Settings">
                                <img src="img/settings-yellow.png" class="cart-img hover1b" alt="Settings" title="Settings">
                            </a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>

                            <!-- <div class="dropdown-content ywllo-dropdown-content">
                                <p class="dropdown-p"><a href="adminProfile.php"  class="menu-padding dropdown-a">Edit Profile</a></p>
                                <p class="dropdown-p"><a href="adminPassword.php"  class="menu-padding dropdown-a hide">Edit Password</a></p>
                                <p class="dropdown-p"><a href="adminBonusReport.php"  class="menu-padding dropdown-a">Bonus Report</a></p>
                                <p class="dropdown-p"><a href="adminWithdrawalReport.php"  class="menu-padding dropdown-a">Withdrawal Report</a></p>
                                <p class="dropdown-p"><a href="adminSignUpProduct.php"  class="menu-padding dropdown-a">Sign Up Product</a></p>
                                <p class="dropdown-p"><a href="adminRate.php" class="menu-padding dropdown-a">Rate</a></p>
                                <p class="dropdown-p"><a href="announcement.php" class="menu-padding dropdown-a">Announcement</a></p>
                                <p class="dropdown-p"><a href="adminReason.php" class="menu-padding dropdown-a">Reason</a></p>
                                <p class="dropdown-p"><a href="logout.php" class="menu-padding dropdown-a">Logout</a></p>
                            </div> -->

                            <div class="dropdown-content ywllo-dropdown-content">
                                <p class="dropdown-p"><a href="adminProfile.php"  class="menu-padding dropdown-a"><img src="img/edit.png" class="cart-img dropdown-img2" alt="<?php echo _MAINJS_HEADER_ADMIN_EDIT_PROFILE ?>" title="<?php echo _MAINJS_HEADER_ADMIN_EDIT_PROFILE ?>"><?php echo _MAINJS_HEADER_ADMIN_EDIT_PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="adminBonusReport.php"  class="menu-padding dropdown-a"><img src="img/document4.png" class="cart-img dropdown-img2" alt="<?php echo _MAINJS_HEADER_ADMIN_BONUS_RPT ?>" title="<?php echo _MAINJS_HEADER_ADMIN_BONUS_RPT ?>"><?php echo _MAINJS_HEADER_ADMIN_BONUS_RPT ?></a></p>
                                <p class="dropdown-p"><a href="adminWithdrawalReport.php"  class="menu-padding dropdown-a"><img src="img/purchase-history.png" class="cart-img dropdown-img2" alt="<?php echo _MAINJS_HEADER_ADMIN_WITHDRAWAL_RPT ?>" title="<?php echo _MAINJS_HEADER_ADMIN_WITHDRAWAL_RPT ?>"><?php echo _MAINJS_HEADER_ADMIN_WITHDRAWAL_RPT ?></a></p>
                                <!-- <p class="dropdown-p"><a href="adminSignUpProduct.php"  class="menu-padding dropdown-a"><img src="img/bottle.png" class="cart-img dropdown-img2" alt="<?php //echo _MAINJS_HEADER_ADMIN_SIGN_UP_PRODUCT ?>" title="<?php //echo _MAINJS_HEADER_ADMIN_SIGN_UP_PRODUCT ?>"><?php //echo _MAINJS_HEADER_ADMIN_SIGN_UP_PRODUCT ?></a></p> -->
                                <p class="dropdown-p"><a href="announcement.php" class="menu-padding dropdown-a"><img src="img/announcement.png" class="cart-img dropdown-img2" alt="<?php echo _MAINJS_HEADER_ADMIN_ANNOUNCEMENT ?>" title="<?php echo _MAINJS_HEADER_ADMIN_ANNOUNCEMENT ?>"><?php echo _MAINJS_HEADER_ADMIN_ANNOUNCEMENT ?></a></p>
                                <p class="dropdown-p"><a href="logout.php" class="menu-padding dropdown-a"><img src="img/logout.png" class="cart-img dropdown-img2" alt="<?php echo _MAINJS_HEADER_ADMIN_LOGOUT ?>" title="<?php echo _MAINJS_HEADER_ADMIN_LOGOUT ?>"><?php echo _MAINJS_HEADER_ADMIN_LOGOUT ?></a></p>
                            </div>

                    </div>
                </div>
                    <div class="float-right right-menu-div admin-mobile">
                            
                            <div id="dl-menu" class="dl-menuwrapper admin-mobile-dl">
                                <button class="dl-trigger">Open Menu</button>
                                    <ul class="dl-menu">
                                        <li><a href="adminDashboard.php"><?php echo _MAINJS_HEADER_ADMIN_DASHBOARD ?></a></li>
                                        <li><a href="adminPayment.php"><?php echo _MAINJS_HEADER_ADMIN_PAYMENT ?></a></li>
                                        <li><a href="adminShipping.php"><?php echo _MAINJS_HEADER_ADMIN_SHIPPING ?></a></li>
                                        <li><a href="adminWithdrawal.php" ><?php echo _MAINJS_HEADER_ADMIN_WITHDRAWAL ?></a></li>
                                        <li><a href="adminProduct.php"><?php echo _MAINJS_HEADER_ADMIN_PRODUCT ?></a></li>
                                        <li><a href="adminMember.php"><?php echo _MAINJS_HEADER_ADMIN_MEMBER ?></a></li>
                                        <li><a href="adminProfile.php"><?php echo _MAINJS_HEADER_ADMIN_EDIT_PROFILE ?></a></li>
                                        <li><a href="adminBonusReport.php"><?php echo _MAINJS_HEADER_ADMIN_BONUS_RPT ?></a></li>
                                        <li><a href="adminWithdrawalReport.php"><?php echo _MAINJS_HEADER_ADMIN_WITHDRAWAL_RPT ?></a></li>
                                        <!-- <li><a href="adminSignUpProduct.php"><?php //echo _MAINJS_HEADER_ADMIN_SIGN_UP_PRODUCT ?></a></li> -->
                                        <li class="title-li"><?php echo _MAINJS_HEADER_ADMIN_LANGUAGE ?></li>
                                        <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a>
                                        <a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>
                                        <li  class="last-li"><a href="logout.php"><?php echo _MAINJS_HEADER_ADMIN_LOGOUT ?></a></li>
                                    </ul>
							</div>

                     </div>
                <?php
                 }
                 else
                 {
                    ?>
                     <div class="right-menu-div float-right">
                        <!-- <a  class="menu-padding white-to-yellow login-a open-login">Login</a>
                        <a  class="menu-padding white-to-yellow div-div open-register">Sign Up</a>             -->
						
                        <a  class="gold-a open-login hover-a before-icon"><?php echo _MAINJS_HEADER_LOGIN ?></a>
                        <div class="dropdown hover1">
                                    <a  class="menu-spacing gold-a"><?php echo _MAINJS_HEADER_ADMIN_LANGUAGE ?></a>
                                    <a  class="dropdown-btn">
                                        <img src="img/dropdown.png" class="dropdown-img" alt="dropdown" title="dropdown">
                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content smaller-dropdown">
                                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a"><?php echo _MAINJS_HEADER_EN ?></a></p>
                                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a"><?php echo _MAINJS_HEADER_CN ?></a></p>
                                    </div>
                        </div> 

 					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                                  <li><a class="menu-padding open-login"><?php echo _MAINJS_HEADER_LOGIN ?></a></li>
                                  <li class="title-li"><?php echo _MAINJS_HEADER_ADMIN_LANGUAGE ?></li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->
                    </div>
                    <?php
                 }
            }
            ?>
        </div>

    </header>
