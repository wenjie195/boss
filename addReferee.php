<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/GroupCommission.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $register_uid = md5(uniqid());
    $register_username = rewrite($_POST['register_username']);

    $register_email_user = null;
    if(isset($_POST['register_email_user']))
    {
        $register_email_user = rewrite($_POST['register_email_user']);
        $register_email_user = filter_var($register_email_user, FILTER_SANITIZE_EMAIL);
    }

    $register_ic_no = rewrite($_POST['register_ic_no']);
    $register_password = $_POST['register_password'];
    $register_password_validation = strlen($register_password);
    $register_retype_password = $_POST['register_retype_password'];

    $register_ic_referrer = $_POST['register_ic_referrer'];
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

//$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/addReferee.php" />
    <meta property="og:title" content="Add Referee | Boss" />
    <title>Add Referee | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/referee.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php echo '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>'; ?>
<?php echo '<script type="text/javascript" src="script.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="username"><?php echo _MAINJS_ADD_REFEREE_TITLE ?></h1>
    
    <form  class="edit-profile-div2" onsubmit="return registerNewMemberAccountValidation();" action="utilities/addNewRefereeFunction.php" method="POST">
        <table class="edit-profile-table password-table">
        	<tr class="profile-tr">
            	<td class=""><?php echo _MAINJS_ADD_REFEREE_USERNAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input class="clean edit-profile-input" type="text" placeholder="Username" id="register_username" name="register_username">
                </td>
            </tr>
            <tr class="profile-tr">
              	<td class=""><?php echo _MAINJS_ADD_REFEREE_FULLNAME ?></td>
                  <td class="profile-td2">:</td>
                  <td class="profile-td3">
                      <input class="clean edit-profile-input" type="text" placeholder="Fullname" id="register_fullname" name="register_fullname">
                  </td>
              </tr>
        	<tr class="profile-tr">
            	<td><?php echo _MAINJS_ADD_REFEREE_IC_NO ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required class="clean edit-profile-input" type="text" placeholder="IC Number" id="register_ic_no" name="register_ic_no">
                </td>
            </tr>
            <tr class="profile-tr">
            	<td><?php echo _MAINJS_ADD_REFEREE_EMAIL ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input class="clean edit-profile-input" type="text" placeholder="Email" id="register_email_user" name="register_email_user">
                </td>
            </tr>

            <input required class="clean edit-profile-input" type="hidden" placeholder="Referrer`s Username." id="register_username_referrer" name="register_username_referrer"
                            value="<?php echo $userDetails->getUsername();?>" readonly>

            <!-- <input required class="clean edit-profile-input" type="text" placeholder="downline_number" id="downline_number" name="downline_number"
                            value="<?php //echo $userDetails->getTotalDownlineNo();?>" readonly>

            <input required class="clean edit-profile-input" type="text" placeholder="bonus" id="bonus" name="bonus"
                            value="<?php //echo $userDetails->getBonus();?>" readonly> -->

          </table>

        <input required class="login-input password-input clean" type="hidden" id="register_password" name="register_password" value="123321">
        <input required class="login-input password-input clean" type="hidden" id="register_retype_password" name="register_retype_password" value="123321">

        <div class="clear"></div>

        <button class="confirm-btn text-center white-text clean black-button"name="refereeButton"><?php echo _MAINJS_ADD_REFEREE_ADD_REFEREE_BUTTON ?></button>
    </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';
}
?>

<script>
  function checkIfVariableIsNullOrEmptyString(field,isValidate)
  {
    if(field == null || field == "" || field.length == 0)
    {
      isValidate += 1;
      return isValidate;
    }
    else
    {
      return isValidate;
    }
  }
  function registerNewMemberAccountValidation()
  {
    // First Level Validation
    let isValidatedAndCanProceedToNextLevel = 0;

    let register_username = $('#register_username').val();
    let register_ic_no = $('#register_ic_no').val();
    let register_password = $('#register_password').val();
    let register_retype_password = $('#register_retype_password').val();
    // let register_email_referrer = $('#register_email_referrer').val();

    // console.log('Initial Counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_username,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_ic_no,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);


    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_password,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_retype_password,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    // Second Level Validation
    if(isValidatedAndCanProceedToNextLevel == 0)
    {
      // alert('can continue');
      if(register_password.length >= 6)
      {
        if(register_password == register_retype_password)
        {
        //   if(register_email_referrer == null || register_email_referrer == "" || register_email_referrer.length == 0)
        //   {

        //   }
        //   else
        //   {
        //     if(emailIsValid (register_email_referrer))
        //     {

        //     }
        //     else
        //     {
        //       alert('Referrer`s email is not valid ! Please try again ! ');
        //       event.preventDefault();
        //     }
        //   }
        }
        else
        {
          alert('Password does not match ! Please try again ! ');
          event.preventDefault();
        }
      }
      else
      {
        alert('Password must be more than 5 ! Please try again ! ');
        event.preventDefault();
      }
    }
    else
    {
      alert('Please enter all fields required ! ');
      event.preventDefault();
    }bbbbbbb
  }
  function emailIsValid (email)
  {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  }
</script>

</body>
</html>
