<?php
class Product{
    var $id, $name, $price, $type,$description,$dateCreated,$dateUpdated,$stock,$display,$images,$buyStock,$totalPriceBuy;

    /**
     * @return mixed
     */
    public function getBuyStock()
    {
        return $this->buy_stock;
    }

    /**
     * @param mixed $id
     */
    public function setBuyStock($buyStock)
    {
        $this->buy_stock = $buyStock;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $id
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $id
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setTotalPrice($totalPriceBuy)
    {
        $this->total_price = $totalPriceBuy;
    }

}

function getProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","name","price","type","description","date_created","date_updated","stock","display","images","buy_stock","total_price");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $name, $price, $type,$description,$dateCreated,$dateUpdated,$stock,$display,$images,$buyStock,$totalPriceBuy);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Product();
            $class->setId($id);
            $class->setName($name);
            $class->setPrice($price);
            $class->setType($type);
            $class->setDescription($description);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            $class->setStock($stock);
            $class->setDisplay($display);
            $class->setImages($images);
            $class->setBuyStock($buyStock);
            $class->setTotalPrice($totalPriceBuy);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createProductList($products,$cartType = 1,$postQuantityRows = null,$isIncludeNotSelectedProductToo = true){
    /*
     * CART TYPE
     * 1 = normal product display cart
     * 2 = checkout cart
     */

    $productListHtml = "";

    if(!$products){
        return $productListHtml;
    }

    $index = 0;
    foreach ($products as $product){
        $quantity = 0;
        if($postQuantityRows){
            $quantity = $postQuantityRows[$index];
        }

        if($quantity <= 0 && !$isIncludeNotSelectedProductToo){
            $productListHtml .= '<div style="display: none;">';
        }else{
            $productListHtml .= '<div style="display: block;">';
        }

        $conn=connDB();
        //$productArray = getProduct($conn);
                  $id  = $product->getName();
              // Include the database configuration file


              // Get images from the database
              $query = $conn->query("SELECT images FROM product WHERE name = '$id'");

              if($query->num_rows > 0){
                  while($row = $query->fetch_assoc()){
                      $imageURL = './ProductImages/'.$row["images"];

        $productListHtml .= '
              <!-- Product -->
                        <br><br><br><div class="left-product-div">
                            <div class="big-product-div">
                                <img src="'.$imageURL.'" class="big-product-css" alt="'.$product->getName().'" title="'.$product->getName().'" >
                            </div>
                        </div>

                        <div class="right-product-div white-text">
                            <h1 class="product-name-h1 gold-text">'.$product->getName().'</h1>
                            <h3 class="product-description">'.$product->getDescription().'</h3>
                            <!-- <a class="open-product black-link-underline">Click here for product details</a><br>-->
                            <h3 class="product-name-h1">RMB '.$product->getPrice().'.00</h3>
                            <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">

                            <div class="input-group">
                                <button type="button" class="button-minus math-button clean">-</button>
                                <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean">
                                <button type="button" class="button-plus math-button clean">+</button>
                            </div>
                        </div>

                        <div class="clear"></div>
                </div>

        ';
      }
    }
        $index++;
    }

    $productListHtml .= '
        <script>
            $(".button-minus").on("click", function(e) {
                e.preventDefault();
                var $this = $(this);
                var $input = $this.closest("div").find("input");
                var value = parseInt($input.val());

                if (value > 1) {
                    value = value - 1;
                } else {
                    value = 0;
                }

              $input.val(value);

            });

            $(".button-plus").on("click", function(e) {
                e.preventDefault();
                var $this = $(this);
                var $input = $this.closest("div").find("input");
                var value = parseInt($input.val());

                if (value < 100) {
                    value = value + 1;
                } else {
                    value = 100;
                }

                $input.val(value);
            });
        </script>
    ';

    return $productListHtml;
}

function getProductPrice($conn,$productId){
    $price = 0;

    $productRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $price = $productRows[0]->getPrice();
    }

    return $price;
}

function addToCart(){
    $shoppingCart = array();

    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

//        if($quantity > 0){
        $thisOrder = array();
        $thisOrder['productId'] = $productId;
        $thisOrder['quantity'] = $quantity;
        array_push($shoppingCart,$thisOrder);
//        }
    }

    if(count($shoppingCart) > 0) {
        $_SESSION['shoppingCart'] = $shoppingCart;
    }

}

function clearCart(){
    unset ($_SESSION["shoppingCart"]);
}

//when checkout only create order
function createOrder($conn,$uid){
    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'] && count($_SESSION['shoppingCart']) > 0){
        $shoppingCart = $_SESSION['shoppingCart'];
        $orderId = insertDynamicData($conn,"orders",array("uid"),array($uid),"s");
        //$orderId = insertDynamicData($conn,"orders",array("uid","username","full_name"),array($uid,$username,$fullName),"sss");

        if($orderId){
            $totalPrice = 0;

            for($index = 0; $index < count($shoppingCart); $index++){
                $thisCart = $shoppingCart[$index];
                $productId = $thisCart['productId'];
                $quantity = $thisCart['quantity'];
                $originalPrice = getProductPrice($conn,$productId);
                $totalPrice += ($originalPrice * $quantity);

                if(!insertDynamicData($conn,"product_orders",array("product_id","order_id","quantity","final_price","original_price","discount_given"),
                    array($productId,$orderId,$quantity,$originalPrice,$originalPrice,0),"iiiddd")){
                    promptError("error creating order for product : $productId");
                }
            }


            //todo this 2 code is AFTER payment successfully done then only execute
//            insertIntoTransactionHistory($conn,$totalPrice,0,$uid,null,null,null,2,null,$orderId,3,null,null);
//            initiateReward($conn,$orderId,$uid,$totalPrice);

        }else{
            promptError("error creating order");
        }

    }
}

function getShoppingCart($conn,$cartType = 2,$isIncludeNotSelectedProductToo = false){
    $productListHtml = "";

    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
        $products = array();
        $quantities = array();
        for($index = 0; $index < count($_SESSION['shoppingCart']); $index++){
            $thisCart = $_SESSION['shoppingCart'][$index];

            $tempProductRows = getProduct($conn," WHERE id = ? ",array("id"),array($thisCart['productId']),"i");
            if($tempProductRows){
                array_push($products,$tempProductRows[0]);
                array_push($quantities,$thisCart['quantity']);
            }
        }

        if(count($products) > 0 && count($quantities) > 0 && count($products) == count($quantities)){
            $productListHtml = createProductList($products,$cartType,$quantities,$isIncludeNotSelectedProductToo);
        }
    }

    return $productListHtml;
}

/*
    WenJie_READ - the shopping cart functions are here
    getShoppingCart is to get the cart with products from th SESSION variable
    createProductList is to get all products from the Product database table and display it
    you can refer to product.php (NOT this file, this file is classes/Product.php) on how to use it
    The idea is that every time user add some product to cart it will store it into session variable.
    Then, after user clicks the checkout button, you use createOrder() function to help create order in the database
    Then user pays the bill (currently database structure doesn't support recording iPay88's data such as receipt id, etc. You need to modify it)
    After paid successful, insert new row into transaction_history to record that the user has paid for an order successfully
    Finally, execute the initiateReward() function to calculate and disperse the commissions accordingly
    You can refer to the example 3 lines of commented code in the createOrder function.
    It is commented because it should only be executed after order is verified and paid successfully via iPay88
*/
