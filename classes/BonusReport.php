<?php
class BossBonusReport {
    /* Member variables */
    var $id,$newUsername,$receiverUsername,$receiverUid,$amount,$dateCreated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNewUsername()
    {
        return $this->newUsername;
    }

    /**
     * @param mixed $newUsername
     */
    public function setNewUsername($newUsername)
    {
        $this->newUsername = $newUsername;
    }

    /**
     * @return mixed
     */
    public function getReceiverUsername()
    {
        return $this->receiverUsername;
    }

    /**
     * @param mixed $receiverUsername
     */
    public function setReceiverUsername($receiverUsername)
    {
        $this->receiverUsername = $receiverUsername;
    }

    /**
     * @return mixed
     */
    public function getReceiverUid()
    {
        return $this->receiverUid;
    }

    /**
     * @param mixed $receiverUid
     */
    public function setReceiverUid($receiverUid)
    {
        $this->receiverUid = $receiverUid;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

        /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;
    }

}

function getBonusReport($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","new_username","receiver_username","receiver_uid","amount","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"boss_bonus");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$newUsername,$receiverUsername,$receiverUid,$amount,$dateCreated);


        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new BossBonusReport();

            $class->setId($id);
            $class->setNewUsername($newUsername);
            $class->setReceiverUsername($receiverUsername);
            $class->setReceiverUid($receiverUid);
            $class->setAmount($amount);
            $class->setDateCreated($dateCreated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
