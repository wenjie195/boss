<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
$conn = connDB();

$getAllUpline = getTop10ReferrerOfUser2($conn,$uid);

for ($cnt=0; $cnt <count($getAllUpline) ; $cnt++) {
  //echo $getAllUpline[$cnt];

  $userDownline = getUser($conn, "WHERE uid =?", array("uid"), array($getAllUpline[$cnt]), "s");

  echo $userDownline[0]->getUsername() ."<br>";
}
